from dotenv import load_dotenv
from dotenv import dotenv_values

load_dotenv()
config = dotenv_values('.env')

print("Welcome to Requestor!")
print("GLOBE ACCESS TOKEN VALUE: {}".format(config.get('GLOBE_ACCESS_TOKEN')))
print("DB_USERNAME: {}".format(config.get('DB_USERNAME')))
print("DB_PASSWORD: {}".format(config.get('DB_PASSWORD')))
print("DB_HOST: {}".format(config.get('DB_HOST')))
print("DB_PORT: {}".format(config.get('DB_PORT')))
